"use strict";

const moment = use("moment");

const MonthlyReport = use('App/Models/MonthlyReport');
const Operation = use('App/Models/Operation');
const Profile = use('App/Models/UserProfile');

const formatHelper = use('App/Helpers/formatHelper');

class MonthlyReportHelper {

	constructor() {}

	static async createMonthlyReport(isCurrent) {

		this.monthlyReport = new MonthlyReport(isCurrent);

		this.reportPeriod = {
			'start': this.monthlyReport.startDate.toDate(),
			'end': this.monthlyReport.endDate.toDate()
		};

		await this.calcActiveBalance();
		await this.calcPassiveBalance();
		await this.calcDeposit();
		await this.calcWithdraw();
		await this.calcWithdrawCorrect();
		await this.calcDepositCorrect();
		await this.calcPnl();
		await this.calcRef();

		return this.monthlyReport;
	}

	static async calcActiveBalance() {
		let currentActiveBalancesSum = await this.getCurrentActiveBalancesSum();
		let activeBalancesDelta = await this.calcActiveBalancesDelta();

		this.monthlyReport.activeBalance = Math.round((currentActiveBalancesSum - activeBalancesDelta) * 100) / 100;
	}

	static async getCurrentActiveBalancesSum() {
		return await Profile
			.query()
			.getSum('total_balance');
	}

	static async calcActiveBalancesDelta() {
		let operations = (await Operation
			.query()
			.where('stock_date', '>', this.reportPeriod.end)
			.with('type')
			.fetch()).rows;

		let delta = 0;
		let amount;
		let direction
		for (let operation of operations) {
			amount = operation.amount;
			direction = operation.type.direction;
			delta += !!direction ? -amount : amount;
		}

		return delta;
	}

	static async calcPassiveBalance() {
		this.monthlyReport.passiveBalance = await Operation
			.query()
			.where('status_id', 8)
			.where('stock_date', '>', this.reportPeriod.start)
			.where('stock_date', '<', this.reportPeriod.end)
			.getSum('amount');
	}

	static async calcDeposit() {
		let depositTypeIds = (2, 6);
		this.monthlyReport.deposit = await this.getSumOnPeriodByType(depositTypeIds);
	}

	static async calcWithdraw() {
		let withdrawTypeId = 3;
		this.monthlyReport.withdraw = await this.getSumOnPeriodByType(withdrawTypeId);
	}

	static async calcWithdrawCorrect() {
		let withdrawCorrectTypeId = 5;
		this.monthlyReport.withdrawCorrect = await this.getSumOnPeriodByType(withdrawCorrectTypeId);
	}

	static async calcDepositCorrect() {
		let depositCorrectTypeId = 7;
		this.monthlyReport.depositCorrect = await this.getSumOnPeriodByType(depositCorrectTypeId);
	}

	static async calcPnl() {
		let pnlTypeId = 1;
		this.monthlyReport.pnl = await this.getSumOnPeriodByType(pnlTypeId);
	}

	static async calcRef() {
		let refTypeId = 4;
		this.monthlyReport.ref = await this.getSumOnPeriodByType(refTypeId);
	}

	static async getSumOnPeriodByType(...typeId) {

		let passiveStatusId = 8;
		return await Operation
			.query()
			.whereIn('type_id', typeId)
			.where('stock_date', '>', this.reportPeriod.start)
			.where('stock_date', '<', this.reportPeriod.end)
			.whereNot('status_id', passiveStatusId)
			.getSum('amount');
	}

}

module.exports = MonthlyReportHelper;
