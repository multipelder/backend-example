"use strict";

const moment = use("moment");

class MonthlyReport {

	constructor(isCurrent=false) {

		this.activeBalance = 0;
		this.passiveBalance = 0;
		this.deposit = 0;
		this.withdraw = 0;
		this.withdrawCorrect = 0;
		this.depositCorrect = 0;
		this.pnl = 0;
		this.ref = 0;

		this.startDate = moment().startOf("month");
		this.endDate = moment();

		if (!isCurrent) {
			this.startDate.subtract(1, "month").startOf("month");
			this.endDate.startOf("month");
		}
	}

}

module.exports = MonthlyReport;
