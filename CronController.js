"use strict";

const Operation = use("App/Models/Operation");
const calendarHelper = use("App/Helpers/calendarHelper");
const t = use("App/Helpers/string-localize");
const monthlyReportHelper = use("App/Helpers/monthlyReportHelper");

const moment = use("moment");

const NotificationController = use('App/Controllers/Common/NotificationController');

class CronController {

	async createAndSendMonthlyReport({ params: { is_current } }) {

		const monthlyReport = await monthlyReportHelper.createMonthlyReport(is_current);

		monthlyReport.title = calendarHelper.getMonthLocalName(monthlyReport.startDate.month()) + ' ' + monthlyReport.startDate.year();
		const subject = t('Сводка за месяц');
		return await NotificationController.sendToBoss(
			monthlyReport,
			'monthly_report',
			subject
		);
	}
}

module.exports = CronController;
