'use strict';

const moment = use('moment');
const t = use("App/Helpers/string-localize");

class CalendarHelper {

    static getNextWorkDay(date) {
        if ( date instanceof Date ) {
            date = this.dateIntoMomentObj(date);
        }

        date = moment(date);
        do { date = date.add(1, 'd'); }
        while (
            !this.isWorkDay(date)
        );

        return date;
    }

    static getPrevWorkDay(date) {
        if ( date instanceof Date ) {
            date = this.dateIntoMomentObj(date);
        }

        date = moment(date);
        do { date = date.add(-1, 'd'); }
        while (
            !this.isWorkDay(date)
        );

        return date;
    }

    static isWorkDay(date) {
        if ( date instanceof Date ) {
            date = this.dateIntoMomentObj(date);
        }

        return date.isoWeekday() != 6 // Saturday
            && date.isoWeekday() != 7 // Sunday
            && !this.isOnHolidaylist(date)
            || this.isOnWorkdaylist(date);
    }

    static isOnHolidaylist(date) {
        let holidayList = this.holidayList;

        for ( let i = 0; i < holidayList.length; i++) {

            let holiday = holidayList[i];

            if ( this.isSameDays(date, holiday) ) {
                return true;
            }
        }

        return false;
    }

    static isOnWorkdaylist(date) {
        let workdayList = this.workdayList;

        for ( let i = 0; i < workdayList.length; i++) {

            let workday = workdayList[i];

            if ( this.isSameDays(date, workday) ) {
                return true;
            }
        }

        return false;
    }

    static isSameDays(date1, date2) {
        return date1.isSame(date2, 'day');
    }

    static dateIntoMomentObj(date) {
        return moment(date).startOf('day');
    }

	static getMonthLocalName(monthNumber) {

		let monthNumberMap = {
			0: t('Январь'),
			1: t('Февраль'),
			2: t('Март'),
			3: t('Апрель'),
			4: t('Май'),
			5: t('Июнь'),
			6: t('Июль'),
			7: t('Август'),
			8: t('Сентябрь'),
			9: t('Октябрь'),
			10: t('Ноябрь'),
			11: t('Декабрь')
		};

		return monthNumberMap[monthNumber];
	}

    static get holidayList() {

        return [
            moment('2019-1-1', 'YYYY-MM-DD'),
            moment('2019-1-2', 'YYYY-MM-DD'),
            moment('2019-1-3', 'YYYY-MM-DD'),
            moment('2019-1-4', 'YYYY-MM-DD'),
            moment('2019-1-7', 'YYYY-MM-DD'),
            moment('2019-1-8', 'YYYY-MM-DD'),
            moment('2019-3-8', 'YYYY-MM-DD'),
            moment('2019-5-1', 'YYYY-MM-DD'),
            moment('2019-5-2', 'YYYY-MM-DD'),
            moment('2019-5-3', 'YYYY-MM-DD'),
            moment('2019-5-9', 'YYYY-MM-DD'),
            moment('2019-5-10', 'YYYY-MM-DD'),
            moment('2019-6-12', 'YYYY-MM-DD'),
            moment('2019-11-4', 'YYYY-MM-DD')
        ];
    }

    static get workdayList() {

        return [];
    }

}

module.exports = CalendarHelper;
